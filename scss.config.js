//Build related includes.
const nodeSass = require('node-sass'),
		fs = require('fs'),
		glob = require('glob'),
		path = require('path'),
		postcss = require('postcss'),
		postcssrc = require('postcss-load-config');

try {
	//cartridgeList: Defaults to the value of package.json's npm_package_config_cartridge.
	//Or pass --env.cartridge=<cartridge-name>, where 'cartridge-name' is a string. Separate each input using commas, no spaces.
	const cartridgeList = (process.env.npm_config_env_cartridge || process.env.npm_package_config_cartridge).split(/(?:,| )+/);

	cartridgeList.forEach(walkStyles);
}
catch (e) {
	console.log('Please either provide a default cartridge target on your package.json ("config": {"cartridge": "cartridge-name"}), or pass the cartridge using --env.cartridge=<cartridge-name>');
}

/**
 * Start the build process for each matches .scss file found in the srcPath.
 */
function walkStyles(cartridgeName) {
	let inputPath = process.env.npm_package_config_styles_inputPath || 'cartridges/{cartridge}/cartridge/scss/**/*.scss',
		outputPath = process.env.npm_package_config_styles_outputPath || 'cartridges/{cartridge}/cartridge/static/{subDirectory}/css/{outputFile}.css';

	inputPath = inputPath.replace(/{cartridge}/g, cartridgeName);
	outputPath = outputPath.replace(/{cartridge}/g, cartridgeName);

	glob(inputPath, {ignore: '**/_*.scss'}, function (err, fileList) {
		fileList.forEach(parseSass.bind(this, inputPath, outputPath));
	});
}

/**
 * Parse the provided SCSS file and output a single file with all the @import directives.
 * Send the new file to runPostcss
 * @param  {[String]} inputPath [description]
 * @param  {[String]} outputPath [description]
 * @param  {[String]} inputFile [description]
 */
function parseSass(inputPath, outputPath, inputFile) {

	let globPatternIndex = inputPath.indexOf('**'),
		fileLocation = globPatternIndex != -1 ? path.dirname(inputFile).substring(globPatternIndex) : '',
		subDirIndex = fileLocation.indexOf('/'),
		subDirPath = subDirIndex != -1 ? fileLocation.substring(0, subDirIndex) : fileLocation;
		// subDirPath = subDirIndex != -1 ? fileLocation.substring(subDirIndex) : ''; //This would break CSS url (i.e. background-image, @font-face, etc.) references to outer folders...

	let outputFileName = path.basename(inputFile, '.scss'),
		outputFile = outputPath.replace(/{subDirectory}/g, subDirPath);

	outputFile = outputFile.replace(/{outputFile}/g, outputFileName);

	nodeSass.render({
		file: inputFile,
		outFile: outputFile,
		sourceMap: true,
		includePaths: ['node_modules']
	}, runPostcss.bind(this, inputFile, outputFile));
}

/**
 * Process the parsed and built SASS file through Postcss [https://www.npmjs.com/package/postcss].
 * Uses the postcss configuration provided on the 'postcss.config.js' file.
 * @param  {[String]} inputFile  [description]
 * @param  {[String]} outputFile [description]
 * @param  {[Object]} err        [description]
 * @param  {[Object]} scssResult [description]
 */
function runPostcss(inputFile, outputFile, err, scssResult) {
	if (err) {
		console.log('\x1b[31m%s\x1b[0m', err);

		return false;
	}

	postcssrc().then(({plugins}) => {
		postcss(plugins)
			.process(
				scssResult.css,
				{
					from: inputFile,
					to: outputFile,
					map: {
							inline: false,
							prev: scssResult.map ? scssResult.map.toString() : false
						}
				}
			).then(writeCss.bind(this, outputFile));
	});
}

/**
 * Recursive check for directory existence and creation.
 * @param  {[String]} filePath [description]
 */
function ensureDirs(filePath) {
	let dirPath = path.dirname(filePath);

	if (fs.existsSync(dirPath)) {
		return true;
	}

	ensureDirs(dirPath);
	fs.mkdirSync(dirPath);
}

/**
 * Outputs the built CSS and creates the corresponding file and its map.
 * @param  {[type]} outputFile [description]
 * @param  {[type]} result     [description]
 * @return {[type]}            [description]
 */
function writeCss(outputFile, result) {
	ensureDirs(outputFile);

	fs.writeFile(outputFile, result.css);

	if (result.map) {
		fs.writeFile(outputFile + '.map', result.map);
		logFile(outputFile + ' and its .map file');
	}

	else {
		logFile(outputFile);
	}
}

//Logs file changes.
function logFile(file) {
	//This weird code is for the green color.
	console.log('\x1b[36m%s\x1b[0m', '\n' + '✔ CSS Finished building: ' + file);
}

