

#BORN build process with Revolver

There are different scenarios covered by Revolver when working on a project with single or multiple store configurations. Revolver adapts to your setup depending on the parameters passed and which files are available.

##Multiple stores configurations with either a main or independent store cartridge(s):

Let’s say you have multiple stores and each has its own styles and own set of JS. To build the default store, all you need to run is:

    npm run [task]

To build a specific store or cartridge, run:

    npm run [task] --env.cartridge=your_cartridge_name

**Note: replace “`[task]`” with the task you want to run, such as `build` or `watch`.**  
  

If the current cartridge you’re building doesn’t have any *app.js* files (inside the *js/* or *js/born/* directories), then said cartridge will build the JS from the default `config.cartridge` value.

If you need one of your cartridges to leverage almost all of the JS functionality from one or more cartridge(s), but want to change a few components or files, you can set `--env.revolver=true` and add the JS files you want to change inside of your cartridge, in the same relative directory as their location in the other cartridges. To do so, run:

    npm run [task] --env.cartridge=your_cartridge_name --env.revolver=true

This is assuming you have a config property in your *package.json* file:

    "scripts":  {
        [...]
    },
    "config":  {
        "cartridge": “your_main_cartridge”,
        "revolverPath": “one_other_cartridge, two_other_cartridge, n_other_cartridge”
    },
    "devDependencies": {
        [...]
    }

If you pass in `--env.revolver=true`, but `revolverPath` is missing, then the build will use the current cartridge(s)  plus the cartridge defined in the `config.cartridge` config, in this example the path would be “`your_current_cartridge, your_main_cartridge`”.

You can override the `resolverPath` during build time by passing it as a parameter:

    npm run [task] --env.cartridge=your_cartridge_name --env.revolverPath=three_other_cartridge,four_other_cartridge

**Note: when passing the `env.revolverPath` parameter in the command, you can omit the `env.revolver` parameter.**  
  

Finally, all of these use cases allow running multiple cartridges in parallel. For example, say you’re working on a multi-site and are changing the main store plus a “themed” store, to do so you’d run:

    npm run [task] --env.cartridge=main_cartridge_name,themed_cartridge_name

**Note: When running a command, do not add spaces between each cartridge name, just a coma (this is a limitation with Node).**  
  

When running in this mode, the build will process the cartridges separately and respect the settings and parameters passed at build time.
  
  
##Single store with single or multiple locales:

Create a *style.scss* file inside the respective locale. If you have more than one locale, `@import` all the partials you need from the default or specific locale, then just replace those that are different for your current locale.

Run:

    npm run [task]

This assumes you've setup a default cartridge and an optional set of styles.inputPath and styles.outputPath (defaults to `"cartridges/{cartridge}/cartridge/scss/**/*.scss"` and `"cartridges/{cartridge}/cartridge/static/{subDirectory}/css/{outputFile}.css"` respectively) on your *package.json*

    "scripts":  {
        [...]
    },
    "config":  {
        "cartridge": “your_main_cartridge”,
        "styles": {
          "inputPath": "cartridges/{cartridge}/cartridge/scss/**/*.scss",
          "outputPath": "cartridges/{cartridge}/cartridge/static/{subDirectory}/css/{outputFile}.css"
        }
    },
    "devDependencies": {
        [...]
    }

###Styles object:
|Property|Description|
|--|--|
|`inputPath`|(optional) is the input path to your *.scss* files. The string uses a combination of interpolation and glob's magic patterns to determine the exact file that needs to be parsed. This property accepts a dynamic value for the `{cartridge}`.|    
|`outputPath`|(optional) is the output path of your built *.css* files. The string works similar to `styles.inputPath`, but in addition to having a `{cartridge}` dynamic value, you can also format your string using the `{subDirectory}` and `{outputFile}` values.|

###Styles object dynamic values:
|Value|Description|
|--|--|
|`{cartridge}`|It's value gets replaced with the cartridge name being processed during build time. For example, if you pass `--env.cartridge=your_main_cartridge,your_second_cartridge`, then the value of `{cartridge}` will become each of the specified cartridges| 
|`{subDirectory}`|It's value will be equal to the sub-directory found by the build when expanding the glob pattern in the  `styles.inputPath`, if any. For example, say your inputPath is `"cartridges/{cartridge}/cartridge/scss/**/*.scss"`, which then becomes `"cartridges/your_main_cartridge/cartridge/scss/en_US/main-styles.scss"`. In this example, the value of `{subDirectory}` will be "en_US". **Note:** Because CSS url references are relative to the file's location, built *.css* files will be placed in the specified output directory and will not carry their input sub-directory structure, other than the base `{subDirectory}`.|
|`{outputFile}`|It's value will be equal to the file name found by the build when expanding the glob pattern in the  `styles.inputPath`, if any. For example, say your inputPath is `"cartridges/{cartridge}/cartridge/scss/**/*.scss"`, which then becomes `"cartridges/your_main_cartridge/cartridge/scss/en_US/main-styles.scss"`. In this example, the value of `{subDirectory}` will be "main-styles".|