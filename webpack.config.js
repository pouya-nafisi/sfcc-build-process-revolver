const webpack = require('webpack');
const path = require('path');
const fs = require('fs');
const revolverPlugin = require('revolver-webpack-plugin');

/**
 * Sets the configuration object per cartridge. Only push configurations if there are app.js files found.
 * @param {[Array]} configList [description]
 * @param {[String]} cartridge  [description]
 */
function setConfiguration(configList, cartridge) {

	let jsPath = getJsPaths(cartridge),
		appPaths = getAppPaths(jsPath),
		//You can set this to 'prod' to disable mapping and enabling minification.
		envType = process.env.npm_config_env_type || 'dev',
		//don't attempt to resolve from other directories if current cartridge is the default.
		useRevolver = cartridge !== process.env.npm_package_config_cartridge && (process.env.npm_config_env_revolver || process.env.npm_config_env_revolverPath);

	//If the current cartridge doesn't have an app.js or born/app.js file,
	//then use the cartridge specified on process.env.npm_package_config_cartridge as the destination. 
	if (!appPaths.length) {
		let defaultJSPath = getJsPaths(process.env.npm_package_config_cartridge);

		//If revolver is enabled, set the jsPath to the current cartridge,
		//otherwise use the default cartridge set in process.env.npm_package_config_cartridge.
		//(jsPath is later used by WebPack to determine the output path)
		jsPath = useRevolver ? jsPath : defaultJSPath;

		appPaths = getAppPaths(defaultJSPath);
	}

	if (appPaths.length) {
		let config = {
				entry: {
					app: appPaths,
					vendor: [
							'es6-promise-promise',
							'@borngroup/born-utilities',
							'@borngroup/born-toggle',
							'lodash/each',
							'lodash/map',
							'lodash/findIndex',
							'lodash/debounce',
							'lodash/includes',
							'imagesloaded',
							'jquery-validation',
							'slick-carousel'
						],
				},
				output: {
					path: path.join(__dirname, jsPath.static),
					filename: '[name].js',
					chunkFilename: '[name].js',
				},
				module: {
					rules: [{
						test: /\.js$/,
						exclude: [/node_modules\/(?!@borngroup)/],
						use: ['babel-loader', 'eslint-loader']
					}]
				},
				resolve: {
					plugins: (function(useRevolver) {
						let plugins = [];

						if (useRevolver) {
							plugins.push(
								new revolverPlugin({
									directoryList: getRevolverPaths(cartridge)
								})
							);
						}

						return plugins;
					})(useRevolver)
				},
				plugins: (function(envType) {
					var plugins = [
						new webpack.optimize.CommonsChunkPlugin({
							name: 'vendor',
							chunks: ['app'] //Only extract common imports from the 'app' entry, to avoid breaking standalone functionality, if needed.
						})
					];

					if(envType === 'prod') {
						plugins.push(
							new webpack.optimize.UglifyJsPlugin({
							    compress: {
							        warnings: false
							    },
							    output: {
							        comments: false,
							    }
							})
						);
					}

					return plugins;
				})(envType),
				devtool: 'cheap-source-map',
				externals: {
					'jquery': 'jQuery'
			}
		};

		//Push back the new config into the configList.
		configList.push(config);
	}
}

/**
 * Sets the paths to JS directories and files for the current cartridge.
 * @param  {[String]} cartridge [description]
 * @return {[Object literal]}           [description]
 */
function getJsPaths(cartridge) {
	const JS_PATH = {};

	JS_PATH.main 		= 'cartridges/' + cartridge + '/cartridge/js';
	JS_PATH.static 		= 'cartridges/' + cartridge + '/cartridge/static/default/js';
	JS_PATH.coreApp 	= JS_PATH.main + '/app.js';
	JS_PATH.customApp 	= JS_PATH.main + '/born/app.js';

	return JS_PATH;
}

/**
 * Sets the app paths in an APP_PATH array only if these files exists.
 * This is to avoid running WebPack on cartridges that don't have app.js files.
 * @param {[Object Literal]} jsPath [description]
 * @return {[Array]}           [description]
 */
function getAppPaths(jsPath) {
	const APP_PATHS = [];

	if (fs.existsSync(jsPath.coreApp)) {
		APP_PATHS.push(path.join(__dirname, jsPath.coreApp));
	}

	if (fs.existsSync(jsPath.customApp)) {
		APP_PATHS.push(path.join(__dirname, jsPath.customApp));
	}

	return APP_PATHS;
}

/**
 * Sets the RevolverPlugin paths into an array to be used when instantiating the plugin.
 * @return {[type]} [description]
 */
function getRevolverPaths(currentCartridge) {
	const REV_ARRAY = [];
	const defaultPath = currentCartridge + ', ' + process.env.npm_package_config_cartridge;
	//Attempt to get the revolverPath from command or package.json, if those are not available then fallback to array: [current-cartridge, default-cartridge].
	const REV_PATH = (process.env.npm_config_env_revolverPath || process.env.npm_package_config_revolverPath || defaultPath).split(/(?:,| )+/);

	REV_PATH.forEach(function(cartridge) {
		REV_ARRAY.push(path.join(__dirname, getJsPaths(cartridge).main));
	});

	return REV_ARRAY;
}

/**
 * Starts the build.
 * @param  {[Object]} env [description]
 * @return {[Array]}     [description]
 */
function webpackInit(env) {
	let cartridgeList = (process.env.npm_config_env_cartridge || process.env.npm_package_config_cartridge).split(/(?:,| )+/),
		configList = [];

	cartridgeList.forEach(setConfiguration.bind(this, configList));

	return configList;
}

module.exports = webpackInit;
