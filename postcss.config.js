module.exports = {
    plugins: {
    	autoprefixer: {},
    	cssnano: {
    		sourcemap: true,
			mergeRules: true
    	}
    },
};